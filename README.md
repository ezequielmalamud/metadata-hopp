![banner](https://metadata.fundacionsadosky.org.ar/media/media/images/HOPP_-_Flyer_web_metadata.png)

# Notebooks

- `Competition_Hopp_EDA_&_FE.ipynb`: Contains the EDA and most of the Feature Engineering, as well as the baseline submission (public score: 401.63)
- `Competition_Hopp_Forcasting_SUBMISSION_0.ipynb`: Contains the first submission using a Linear Regressor (public score: 358.50)
- `Competition_Hopp_Forcasting_SUBMISSION_1.ipynb`: Contains the second and final submission using a Random Forest Regressor (public score: 355.21)